#!/bin/python

from flask import Flask, request
from flask_cors import CORS

from game import generate_questions, generate_random_letter, generate_game_id

app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

games = {}
question_file = "../questions.txt"
duration = 120
validCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


@app.route('/api/v1/game', methods=['POST'])
def start_new_game():
    # TODO: this will have a problem with concurrency,
    #  when we happen to get the same game_id twice, it will not be reserved
    game_id = generate_game_id(games.keys())

    request_data = request.get_json()
    if request_data \
            and request_data['letter'] \
            and request_data['letter'] in validCharacters:
        random_letter = request_data['letter']
    else:
        random_letter = generate_random_letter()
    questions = generate_questions(question_file)
    new_game = {
        "id": game_id,
        "letter": random_letter,
        "duration": duration,
        "questions": questions
    }
    games[game_id] = new_game
    return new_game


@app.route('/api/v1/game/<game_id>')
def join_game(game_id):
    return games[game_id]


if __name__ == '__main__':
    app.run(debug=True)
