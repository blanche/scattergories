from unittest import TestCase

from app import start_new_game, join_game, app


class Test(TestCase):
    def test_start_new_game(self):
        with app.test_request_context():
            game = start_new_game()
        self.assertIsNotNone(game['id'])
        returned_game = join_game(game['id'])
        self.assertEqual(game, returned_game)
