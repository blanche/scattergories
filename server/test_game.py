from unittest import TestCase

from game import generate_questions, generate_game_id


class Test(TestCase):
    def test_generate_game_id(self):
        game_id = generate_game_id()
        self.assertIsNotNone(game_id)
        game_id = int(game_id)
        self.assertIsInstance(game_id, int)
        self.assertGreaterEqual(game_id, 100)
        self.assertLessEqual(game_id, 999)

    def test_generate_question(self):
        questions = generate_questions("../questions.txt", num=10)
        # create set of questions to verify that there are no duplicates
        questions = set(questions)
        self.assertIs(len(questions), 10)
        for q in questions:
            self.assertGreater(len(q), 1)
