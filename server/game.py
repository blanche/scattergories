import random
import string


def generate_questions(file, num=12):
    with open(file) as f:
        lines = [x.strip() for x in f.readlines()]
        return random.sample(lines, k=num)


def generate_random_letter():
    random_letter = random.choice(string.ascii_uppercase)
    return random_letter


def generate_game_id(existing_keys=None):
    if existing_keys is None:
        existing_keys = []

    randint = str(random.randint(100, 999))
    while randint in existing_keys:
        randint = generate_game_id()
    return randint
