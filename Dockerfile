FROM python:3.8-alpine

# install nginx, uwsgi and supervisord
RUN apk add --update nginx uwsgi-python3 supervisor && rm -rf /var/cache/apk/* \
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

# install python requirements
ADD server/requirements.txt /server/requirements.txt
RUN pip install -r ./server/requirements.txt

ENV PYTHONPATH $PYTHONPATH:/usr/local/lib/python3.8/site-packages:/usr/lib/python3.8/site-packages

# remove default config of nginx (just returning 404 for /)
RUN rm /etc/nginx/conf.d/default.conf
# add custom nginx coniguration
# ADD docker/main.nginx.conf /etc/nginx/nginx.conf
ADD docker/nginx.conf /etc/nginx/conf.d/
# precautionally create a directory for nginx PID file
RUN mkdir -p /run/nginx

# add supervisord configuration (starting uwsgi and nginx in the background)
ADD docker/supervisord.ini /etc/supervisor.d/
ADD docker/uwsgi.ini /etc/uwsgi/

ADD server /server
ADD questions.txt /
ADD web-app/build /var/lib/nginx/html

CMD ["/usr/bin/supervisord"]