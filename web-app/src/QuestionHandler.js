import questionFile from "./questions.json";
const allQuestions = questionFile.questions;

/* global BigInt */

const encodingString =
  "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_-";
const encodingSize = 6;
const numQuestions = 12;

function getRandomIndicies(numQuestions, sampleSize) {
  if (numQuestions < sampleSize) {
    throw Error("sample size larger than numQuestions");
  }
  const sample = [];
  while (sample.length < sampleSize) {
    const randomIndex = Math.floor(Math.random() * numQuestions);
    if (!sample.includes(randomIndex)) {
      sample.push(randomIndex);
    }
  }
  return sample;
}

function loadGameById(gameId) {
  let indicies = decodeIndicies(gameId.substring(1));
  var selectedQuestions = [];
  for (var i of indicies) {
    selectedQuestions.push(allQuestions[i]);
  }

  return { gameId: gameId, questions: selectedQuestions, letter: gameId[0] };
}

function newGame(letter) {
  const indicies = getRandomIndicies(allQuestions.length, numQuestions);
  const gameId = letter + encodeIndicies(indicies);

  console.log("Generated new game: " + gameId + " [" + indicies + "]");
  var selectedQuestions = [];
  for (var i of indicies) {
    selectedQuestions.push(allQuestions[i]);
  }
  return { gameId: gameId, questions: selectedQuestions, letter: letter };
}

function encodeIndicies(indicies) {
  // convert indicies from 0 to 1 indexed (leading zero issue), convert to binary string
  const joinedIndicies = indicies
    .map((e) => (e + 1).toString().padStart(2, "0"))
    .join("");

  // convert incoming number to binary string
  let binaryNumber = BigInt(joinedIndicies).toString(2);

  // Pad the binary number with zeros to make its length a multiple of size
  while (binaryNumber.length % encodingSize !== 0) {
    binaryNumber = "0" + binaryNumber;
  }

  let result = "";
  // Convert binary to hexadecimal in chunks of size bits
  for (let i = 0; i < binaryNumber.length; i += encodingSize) {
    const chunk = binaryNumber.substring(i, i + encodingSize);
    // convert binary chunk to decimal number
    const dec = parseInt(chunk, 2);
    result += encodingString[dec];
  }

  return result;
}

function decodeIndicies(inputString) {
  let binaryNumber = "";
  for (let i = 0; i < inputString.length; i++) {
    const char = inputString[i];
    const pos = encodingString.indexOf(char);
    binaryNumber += pos.toString(2).padStart(encodingSize, "0");
  }
  let decimalNumber = BigInt("0b" + binaryNumber).toString();
  // add leading zeros before matching below
  while (decimalNumber.length % 2 !== 0) {
    decimalNumber = "0" + decimalNumber;
  }
  // split decimal into pairs of two, convert to int, and convert from 1 to 0 indexed
  let indicies = decimalNumber.match(/(..?)/g).map((x) => parseInt(x) - 1);
  return indicies;
}

export { newGame, encodeIndicies, decodeIndicies, getRandomIndicies, loadGameById };
