import React, { useState, useEffect } from "react";
import "./App.css";
import Game from "./components/game";
import Start from "./components/start";
import { Routes, Route, useNavigate } from "react-router-dom";
import { newGame, loadGameById } from "./QuestionHandler";

const App = () => {
  const generateRandomLetter = () => {
    var randomChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return randomChars.charAt(Math.floor(Math.random() * randomChars.length));
  };

  const [questions, setQuestions] = useState([]);
  const [letter, setLetter] = useState(generateRandomLetter());
  const [gameId, setGameId] = useState();
  const navigate = useNavigate();

  useEffect(() => {
    // check if we came here from a different path
    let pathmatch = window.location.pathname.match(/play\/(.+)/);
    if (pathmatch && pathmatch.length === 2) {
      let gameIdInPath = pathmatch[1];
      if (gameIdInPath !== gameId) {
        let game = loadGameById(gameIdInPath);
        setLetter(game.letter)
        setQuestions(game.questions);
        setGameId(game.gameId);
      }
    }
  }, [gameId]);

  const handleRandomizeLetter = () => {
    setLetter(generateRandomLetter());
  };

  const startGame = () => {
    let game = newGame(letter);
    setLetter(game.letter)
    setQuestions(game.questions);
    setGameId(game.gameId);
    navigate("/play/" + game.gameId);
  };

  const joinGame = (event) => {
    event.preventDefault();
    let gameId = event.target.gameJoinId.value;

    let game = loadGameById(gameId);
    setLetter(game.letter)
    setQuestions(game.questions);
    setGameId(game.gameId);

    navigate("/play/" + gameId);
  };

  return (
    <Routes>
      <Route
        path={"/play/:id"}
        element={<Game questions={questions} letter={letter} gameId={gameId} />}
      />
      <Route
        path="/"
        element={
          <Start
            letter={letter}
            handleRandomizeLetter={handleRandomizeLetter}
            handleStart={startGame}
            handleJoin={joinGame}
          />
        }
      />
    </Routes>
  );
};

export default App;
