import React from 'react'

const Start = (props) => {
    return (
    <center className="mt-5">
        <h1>Scattergories Online</h1>

        <div className="mt-5">
            <h5>Host a game for your friends</h5>
            <div className="d-flex flex-row justify-content-center align-items-center mb-2 mt-3">
                <div className="d-flex justify-content-center">starting letter:&nbsp;<strong>{props.letter}</strong></div>
                <div className="d-flex justify-content-center ms-2" onClick={props.handleRandomizeLetter}>
                    <svg className="bi bi-arrow-clockwise" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" d="M3.17 6.706a5 5 0 0 1 7.103-3.16.5.5 0 1 0 .454-.892A6 6 0 1 0 13.455 5.5a.5.5 0 0 0-.91.417 5 5 0 1 1-9.375.789z"/>
                        <path fillRule="evenodd" d="M8.147.146a.5.5 0 0 1 .707 0l2.5 2.5a.5.5 0 0 1 0 .708l-2.5 2.5a.5.5 0 1 1-.707-.708L10.293 3 8.147.854a.5.5 0 0 1 0-.708z"/>
                    </svg>
                 </div>

            </div>
                <div>
                    <button type="button" className="btn btn-success" onClick={props.handleStart}>Host Game</button>
                </div>
        </div>

        <div className="mt-4" style={{borderTop: "1px solid lightgray", width: "100%"}}></div>

        <div className="mt-4">
            <h6>Join a friend in an existing game</h6>
            <form onSubmit={props.handleJoin}>
                <div className="input-group" style={{width: "33%", minWidth: "220pt"}}>
                    <input type="number" className="form-control" placeholder="Enter game ID" name="gameJoinId" id="gameJoinId" />
                    <div className="input-group-append">
                        <button id="gameJoinButton" className="btn btn-success" type="submit">Join Game</button>
                    </div>
                </div>
            </form>
        </div>
        <div className="mt-5 text-small">
            <small className="text-muted">Created with love by ΛW - check it out on <a href="https://gitlab.com/blanche/scattergories/">GitLab</a></small>
        </div>
    </center>
    )
};

export default Start