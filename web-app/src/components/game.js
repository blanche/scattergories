import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Navbar from 'react-bootstrap/Navbar';
import Form from 'react-bootstrap/Form';
import QRCode from "react-qr-code";

const initialDuration = 120;

const Game = ({ questions, gameId, letter }) => {
    const [answers, setAnswers] = useState('');
    const [started, setStarted] = useState(false);
    const [finished, setFinished] = useState(false);
    const [points, setPoints] = useState(false);
    const [duration, setDuration] = useState(initialDuration);
    const [showTimesUp, setShowTimesUp] = useState(true);
    const [showUrlCopied, setShowUrlCopied] = useState(false);

    const handleAnswerChange = (event) => {
        setAnswers({
            ...answers,
            [event.target.name]: event.target.value
        });
    }
    React.useEffect(() => {
        duration > 0 && setTimeout(() => {
            if (started) {
                setDuration(duration - 1);
            }
            if (duration <= 1) {
                setFinished(true)
            }
        }, 1000);
    }, [duration, started]);

    const startGame = () => {
        setStarted(true);
        setFinished(false);
        setShowTimesUp(true);
        setDuration(initialDuration);
    }

    const handlePointsChange = (event) => {
        setPoints({
            ...points,
            [event.target.name]: parseInt(event.target.value)
        });
    }

    const handleShare = () => {
        if (navigator.share) {
            navigator.share({
              title: 'Scattergories Game #' + gameId,
              url: window.location.href
            }).then(() => {
              console.log('shared ' + window.location.href);
            })
            .catch(console.error);
        }
        return false;
    }

    const handleShowUrlCopied = () => {
        navigator.clipboard.writeText(window.location.href)
        setShowUrlCopied(true)

        setTimeout(() => {
            setShowUrlCopied(false)
        }, 3000)
    }

    return (
        <div>
            <Navbar fixed="top" className="justify-content-between ps-2 pe-2">
                <Navbar.Brand>Letter: <strong>{letter}</strong></Navbar.Brand>
                <div style={{display: finished ? "none" : null}}>Timer: {duration}</div>
                <div style={{display: !finished ? "none" : null}}>Points: {Object.values(points).reduce((a, b) => a + b, 0)}</div>
            </Navbar>

            <div style={{paddingTop:"65px"}}>
            <form>
                {questions.map((question, index) => (
                <div key={"card" + index} className="card">
                  <div className="card-body">
                      <div className="card-title">
                            {index+1}) <span style={{background: !started && "#212529"}}>{question}</span>
                       </div>
                    <input disabled={finished} name={"answerValue" + index} className="form-control" onChange={handleAnswerChange}/>
                    <div className={"mt-3 d-flex flex-row justify-content-around " + (!finished && "d-none")}>
                        <Form.Check onChange={handlePointsChange} value="0" label="0 points" type="radio" name={`points-${index}`} id={`points-${index}-0`} />
                        <Form.Check onChange={handlePointsChange} value="1" label="1 points" type="radio" name={`points-${index}`} id={`points-${index}-1`} />
                        <Form.Check onChange={handlePointsChange} value="2" label="2 points" type="radio" name={`points-${index}`} id={`points-${index}-2`} />
                    </div>
                  </div>
                </div>
                ))}
            </form>
            </div>

            {/* removing animation due to https://github.com/react-bootstrap/react-bootstrap/issues/5075 */}
            <Modal show={!started} keyboard={false} animation={false}>
                <Modal.Header>
                    <Modal.Title>Waiting for other Players</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <p>Invite other players using the Game ID: <strong>{gameId}</strong></p>
                    <p>
                    <button type="button" onClick={handleShowUrlCopied} className="btn btn-outline-success btn-sm ">{showUrlCopied?"URL copied!":"Copy Game URL"} <img alt="copy" src="/copy.svg" /></button>
                    <button type="button" onClick={handleShare} style={{display: !navigator.share && "none"}} className="btn btn-outline-success btn-sm">Share <img alt="share" src="/share-black-18dp.svg" /></button>
                    </p>
                    <div className="mb-3 text-center">
                        Share game URL with this QR Code:
                    <QRCode value={window.location.href}/>
                    </div>
                    <div>The game duration will be <strong>{duration}</strong> seconds.<br/>
                    The letter will be <strong>{letter}</strong></div>
                </Modal.Body>

                <Modal.Footer>
                    <div>Click start when all players have joined</div>
                    <button type="button" className="btn btn-success" onClick={startGame}>Start Game</button>
                </Modal.Footer>
            </Modal>

             <Modal show={finished && showTimesUp} keyboard={false} animation={false}>
                <Modal.Header>
                    <Modal.Title>Times up!</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <p>Read out your answers in your group and calculate your points.</p>

                    <ul>
                        <li>1 point for every answer starting with the given letter which no other player has given</li>
                        <li>2 points for compound terms starting with the same letter (&quot;<strong>J</strong>ack <strong>J</strong>ohnson&quot;)</li>
                    </ul>
                </Modal.Body>

                <Modal.Footer>
                    <button type="button" className="btn btn-success" onClick={() => {setShowTimesUp(false)}}>Got it!</button>
                </Modal.Footer>
            </Modal>
        </div>
    )
};

export default Game