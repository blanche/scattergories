import {
  newGame,
  encodeIndicies,
  decodeIndicies,
  getRandomIndicies,
} from "./QuestionHandler.js";
import questionFile from "./questions.json";
const allQuestions = questionFile.questions;

test("generates Questions", () => {
  const letter = "X";
  var questions = newGame(letter);
  // assert that questions is a list of numbers with the length of 12
  expect(questions.gameId[0]).toEqual(letter);
  // console.log(questions);
  // TODO do some more sanity checks
});

test("check encodeing/decoding", () => {
  for (var i = 0; i < 10; i++) {
    var indicies = getRandomIndicies(allQuestions.length, 12);
    var gameId = encodeIndicies(indicies);
    var indiciesDecoded = decodeIndicies(gameId);
    // console.log(indicies + " / "+gameId+" / " + indiciesDecoded)
    expect(indicies).toEqual(indiciesDecoded);
  }
});
