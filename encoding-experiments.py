import random

encodingString = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_-"
questionsFile = './questions.txt'
sampleSize = 12


def encode(number, size=6):
    # convert incoming number to binary string
    binary_number = str(bin(int(number)))[2:]
    result = ""
    # Pad the binary number with zeros to make its length a multiple of n
    while len(binary_number) % size != 0:
        binary_number = '0' + binary_number

    # Convert binary to hexadecimal in chunks of n bits
    for i in range(0, len(binary_number), size):
        chunk = binary_number[i:i + size]
        # convert integer chunk to decimal number
        dec = int(chunk, 2)
        result += encodingString[dec]

    return result


def decode(input_string, size, output_len=0):
    result = ""
    for char in input_string:
        pos = encodingString.find(char)
        result += f'{{0:0{size}b}}'.format(pos)

    if output_len > 0:
        result = result[-output_len:].zfill(output_len)  # make sure the output is the right lenght if needed
    return result


def oneHotEncoding(sample):
    print("#############################")
    print("oneHot encoding")
    oneHot = numQuestions * ["0"]
    for index in sample:
        oneHot[index] = "1"
    ohs = "".join(oneHot)
    print("binary:        " + ohs)
    print(f"decimal:       {int(ohs, 2)}")
    print(f"hexadecimal:   {hex(int(ohs, 2))}")
    print(f"5-bit:         {encode(int(ohs, 2), 5)}")
    print(f"6-bit:         {encode(int(ohs, 2), 6)}")
    decoded_binary = decode(encode(int(ohs, 2), 6), 6, numQuestions)
    print(f"decoded:       {decoded_binary}")
    print("binary:        " + ohs)
    print("#############################")


def indexEncoding(sample):
    print("#############################")
    print("index encoding")
    padded_sample = ['{0:02}'.format(num) for num in sample]
    print(f"padded_sample {padded_sample}")
    concat_index = "".join(padded_sample)
    print(f"concat_index   {concat_index}")
    print(f"binary         {str(bin(int(concat_index)))}")
    print(f"6-bit:         {encode(concat_index, 6)}")
    decoded_binary = decode(encode(concat_index, 6), 6)
    print(f"decoded:       {decoded_binary}")
    x = int(decoded_binary, 2)
    x = f'{x:024}'
    print(f"decimal:       {x}")
    print(f"concat_index   {concat_index}")
    print("#############################")


with open(questionsFile) as f:
    questions = f.readlines()
    numQuestions = len(questions)
    print(f"numQuestions: {numQuestions}")
    print(f"questions: {questions}")

    sample = random.sample(range(numQuestions), k=sampleSize)
    print(f"sample {sample}")

    indexEncoding(sample)

    oneHotEncoding(sample)
